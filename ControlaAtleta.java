package cadastro.controller;
import java.util.ArrayList;

import cadastro.model.*;

public class ControlaAtleta {

	public ArrayList<Atleta> listaAtleta;
	
	public ControlaAtleta(){
		listaAtleta = new ArrayList<Atleta>();
	}
	
	public void adicionar(Atleta atleta){
		listaAtleta.add(atleta);
		System.out.println("Atleta adicionado.");
	}
	public void remover(Atleta atleta){
		listaAtleta.remove(atleta);
		System.out.println("Atleta removido.");
	}
	public Atleta pesquisarAtleta(String nomeAtleta){
		for(Atleta atleta : listaAtleta){
			if(atleta.getNome().equalsIgnoreCase(nomeAtleta)){
				return atleta;
			}
		}
		return null;
	}
	public void atletasNaLista(){
		for(Atleta atleta : listaAtleta){
			System.out.println(atleta.getNome());
		}
	}
	
}
