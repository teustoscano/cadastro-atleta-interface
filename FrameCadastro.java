package cadastro.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;

import cadastro.controller.ControlaAtleta;
import cadastro.model.Atleta;
import cadastro.model.Endereco;
import cadastro.model.PilotoF1;

import java.awt.Font;
import java.awt.List;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import java.awt.Toolkit;

import javax.swing.JComboBox;

import java.awt.TextArea;

public class FrameCadastro extends JFrame {

	private ControlaAtleta controles;
	private Atleta atleta;
	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtIdade;
	private JTextField txtEquipe;
	private JTextField txtPais;
	private JTextField txtEstado;
	private JTextField txtNPiloto;
	private JTextField txtNPiloto2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameCadastro frame = new FrameCadastro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameCadastro() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Matheus\\Pictures\\formula1-logo2.png"));
		setTitle("FIA-Cadstro de Pilotos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 772, 461);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel labelImage = new JLabel("New label");
		labelImage.setBounds(10, 11, 193, 396);
		
		ImageIcon imagem = new ImageIcon(FrameCadastro.class.getResource("/cadastro/img/formula-1-logo.jpg"));
		Image logo = imagem.getImage().getScaledInstance(labelImage.getWidth(), labelImage.getHeight(), Image.SCALE_DEFAULT);
		
		labelImage.setIcon(new ImageIcon(logo));
		
		contentPane.add(labelImage);
		
		
		
		final TextArea textArea = new TextArea();
		textArea.setEditable(false);
		textArea.setBounds(478, 163, 111, 244);
		contentPane.add(textArea);
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adicionar();
			}
		});
		btnAdicionar.setBounds(213, 53, 111, 99);
		contentPane.add(btnAdicionar);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			remover();
			}
		});
		btnRemover.setBounds(344, 53, 111, 99);
		contentPane.add(btnRemover);
		
		JButton btnListar = new JButton("Listar");
		btnListar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			listar(textArea);
			}
		});
		btnListar.setBounds(478, 53, 111, 99);
		contentPane.add(btnListar);
		
		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			pesquisar();
			}
		});
		btnPesquisar.setBounds(613, 53, 111, 99);
		contentPane.add(btnPesquisar);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(213, 163, 111, 244);
		contentPane.add(panel);
		
		JLabel lblNome = new JLabel("Nome");
		panel.add(lblNome);
		
		txtNome = new JTextField();
		panel.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblIdade = new JLabel("Idade");
		panel.add(lblIdade);
		
		txtIdade = new JTextField();
		panel.add(txtIdade);
		txtIdade.setColumns(10);
		
		JLabel lblEquipe = new JLabel("Equipe");
		panel.add(lblEquipe);
		
		txtEquipe = new JTextField();
		panel.add(txtEquipe);
		txtEquipe.setColumns(10);
		
		JLabel lblPais = new JLabel("Pais");
		panel.add(lblPais);
		
		txtPais = new JTextField();
		panel.add(txtPais);
		txtPais.setColumns(10);
		
		JLabel lblEstado = new JLabel("Estado");
		panel.add(lblEstado);
		
		txtEstado = new JTextField();
		panel.add(txtEstado);
		txtEstado.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(344, 163, 111, 244);
		contentPane.add(panel_1);
		
		JLabel lblNomeDoPiloto_1 = new JLabel("Nome do Piloto");
		panel_1.add(lblNomeDoPiloto_1);
		
		txtNPiloto = new JTextField();
		panel_1.add(txtNPiloto);
		txtNPiloto.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.LIGHT_GRAY);
		panel_3.setBounds(613, 163, 111, 244);
		contentPane.add(panel_3);
		
		JLabel lblNomeDoPiloto = new JLabel("Nome do Piloto");
		panel_3.add(lblNomeDoPiloto);
		
		txtNPiloto2 = new JTextField();
		panel_3.add(txtNPiloto2);
		txtNPiloto2.setColumns(10);
		
		JLabel lblFdrationInternationaleDe = new JLabel("F\u00E9d\u00E9ration Internationale de I'Automobile");
		lblFdrationInternationaleDe.setFont(new Font("Plantagenet Cherokee", Font.ITALIC, 21));
		lblFdrationInternationaleDe.setBounds(213, 11, 402, 31);
		contentPane.add(lblFdrationInternationaleDe);
		

	
		this.controles = new ControlaAtleta();
	}
	
	private void adicionar(){
		
			Atleta atleta = new Atleta();
			PilotoF1 piloto = new PilotoF1();
			atleta.setCategoria(piloto);
			String nome = txtNome.getText();
			atleta.setNome(nome);
			String idade = txtIdade.getText();
			atleta.setIdade(Integer.parseInt(idade));
			String equipe = txtEquipe.getText();
			atleta.getCategoria().setEquipe(equipe);
			Endereco endereco = new Endereco();
			String pais = txtPais.getText();
			endereco.setPais(pais);
			String estado = txtEstado.getText();
			endereco.setEstado(estado);
			
			if((txtNome.getText() == null) || (txtIdade.getText() == null )  || (txtEquipe.getText() == null) ||( txtPais.getText() == null)){
				JOptionPane.showMessageDialog(null, "Todos os campos devem estar preenchidos!");
				adicionar();
			}
			atleta.setEndereco(endereco);
			controles.adicionar(atleta);
			JOptionPane.showMessageDialog(null, "Piloto adicionado!");
			txtNome.setText(null);
			txtIdade.setText(null);
			txtEquipe.setText(null);
			txtPais.setText(null);
			txtEstado.setText(null);
	}
	private void remover(){
		String nomeAtleta = txtNPiloto.getText();
		atleta = controles.pesquisarAtleta(nomeAtleta);
		controles.remover(atleta);
		txtNPiloto.setText(null);
		JOptionPane.showMessageDialog(null, "Piloto Removido!");
		
	}
	private void listar(TextArea listaNome){
		for(Atleta atleta : controles.listaAtleta){
			listaNome.setText(controles.listaAtleta.toString());
			//System.out.println(atleta.getNome());
		}
	}
	private void pesquisar(){
		String nomePiloto = txtNPiloto2.getText();
		atleta = controles.pesquisarAtleta(nomePiloto);
		if(atleta == null){
			JOptionPane.showMessageDialog(null, "Piloto não encontrado!");
		}else{
			JOptionPane.showMessageDialog(null, atleta.getNome()+" | "+atleta.getIdade()+" | "+atleta.getCategoria().getEquipe()+" | "+atleta.getEndereco().getEstado()+" | "+atleta.getEndereco().getPais());
			//System.out.println(atleta.getNome());
		//System.out.println(atleta.getCategoria().getEquipe());
		//System.out.println(atleta.getEndereco().getPais());
			txtNPiloto2.setText(null);
	}
}
}
